<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
*
* @author 	    Brayan Acebo
* @package 	PyroCMS
* @subpackage 	Products
* @category 	Modulos
*/

class Products extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $models = array(
            "product_model",
            "product_category_model",
            "product_image_model",
            "product_intro_model"
            );
        $this->load->model($models);
    }

// -----------------------------------------------------------------

    public function index($selCategory = null)
    {
        $category = null;
        if($selCategory){
            $category = $this->product_category_model->get_many_by("slug",$selCategory);
            if (count($category) > 0) {
            $category = $category[0]; // Categoria Seleccionada

            $return = $this->db->where('category_id',$category->id)->get('products_categories')->result();
            $selected_category = array();
            $products= array();
            $selected_products = array();

            foreach ($return as $item) {
                $selected_products[] = $item->product_id;
            }

            if(count($selected_products) > 0){
                for ($i=0;$i<count($selected_products);$i++) {
                // Se consulta producto a producto de la categoria
                    $product = $this->product_model->get_many_by("id",$selected_products[$i])[0];
                // Llenamos el nuevo objeto con todos los productos
                    $productsObj = new stdClass();
                    $productsObj->id = $product->id;
                    $productsObj->name = substr($product->name, 0, 20);
                    $productsObj->image = val_image($product->image);
                    $productsObj->introduction = substr($product->introduction, 0, 100);                   
                    $productsObj->slug = $product->slug;
                    $productsObj->url = site_url('products/detail/'.$product->slug);
                $products[] = $productsObj; // Array de objectos
            }

                // Se ordena el Array por id de forma DESC
            usort($products, function($a, $b)
            {
                return strcmp($b->id, $a->id);
            });
        }

    }else{
                redirect("products"); // si la categoria no existe me redirecciona a todos
            }
        }else{

        // Se consultan los productos
            $products = $this->product_model
            ->order_by('id', 'DESC')
            ->get_all();

            foreach($products AS $item)
            {
                $item->name = substr($item->name, 0, 20);
                $item->image = val_image($item->image);
                $item->introduction = substr($item->introduction, 0, 100);          
                $item->url = site_url('products/detail/'.$item->slug);
            }
        }

    // Consultamos las categorias
        $categories = $this->product_category_model
        ->order_by('title', 'ASC')
        ->get_all();

    // Intro
        $in = $this->product_intro_model->get_all();
        $intro = array();
        if (count($in) > 0) {
            $intro = $in[0];
        }

    // Devuelve arbol en HTML, el segundo parametro es el nombre del modulo
        $menu = treemenu($categories,'products');

        $this->template
        ->set('products', $products)
        ->set('category', ($category) ? "/ ".$category->title : null)
        ->set('categories', $categories)
        ->set('menu', $menu)
        ->set('intro', $intro)
        ->build('index');
    }


// ----------------------------------------------------------------------

    public function detail($slug)
    {

        $return = $this->product_model->get_many_by('slug', $slug)[0];

        if(!$return)
            redirect('products');

        // Se convierten algunas variables necesarias para usar como slugs
        $setter = array(
            'image' => val_image($return->image)
           // 'price' => ($return->price) ? "Precio: $".number_format($return->price) : null
            );

        $product = array_merge((array)$return,$setter);

        $relation = $this->db->where('product_id',$product['id'])->get('products_categories')->result();
        $categories = array();
        foreach ($relation as $item) {
            $category = $this->product_category_model->get_many_by('id', $item->category_id)[0];
            $categories[] = array(
                    "title" => $category->title,
                    "slug" => $category->slug
                );
        }

        // imagenes para slider
        $images = $this->product_image_model->get_many_by('product_id',$product['id']);

        $this->template
                ->set('product', (object) $product)
                ->set('categories', $categories)
                ->set('images', $images)
                ->build('detail');

    }
}
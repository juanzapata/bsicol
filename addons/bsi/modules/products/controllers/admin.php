<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Brayan Acebo
 */

// Ajustamos Zona Horaria
date_default_timezone_set("America/Bogota");

class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('products');
        $this->template
        ->append_js('module::developer.js')
        ->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
        $models = array(
            "product_model",
            "product_category_model",
            "product_image_model",
            "product_intro_model"
            );
        $this->load->model($models);
    }

    // -----------------------------------------------------------------

    public function index() {

        // Paginación de Productos
        $pagination = create_pagination('admin/products/index', $this->product_model->count_all(), 10);

        // Se consultan los productos
        $products = $this->product_model
        ->order_by('id', 'DESC')
        ->limit($pagination['limit'], $pagination['offset'])
        ->get_all();

        // Consultamos las categorias
        $categories = $this->product_category_model
        ->order_by('id', 'DESC')
        ->get_all();

        foreach ($categories as $key => $value) {
            $parent = $value->parent;
            if($parent != 0){
                $parent_name = $this->product_category_model->get($parent)->title;
                if($parent_name != "") {
                    $categories[$key]->parent_name = $parent_name;
                }
            } else {
                $categories[$key]->parent_name = "";
            }
        }

        // Intro
        $in = $this->product_intro_model->get_all();
        $intro = array();

        if (count($in) > 0) {
            $intro = $in[0];
        }

        $this->template
        ->set('pagination', $pagination)
        ->set('products', $products)
        ->set('categories', $categories)
        ->set('intro', $intro)
        ->build('admin/index');
    }
 public function estado($estado,$id){



        $this->load->model('product_model');

        $this->db->select('estado');

        $this->db->where('estado', 1);

        $this->db->from($this->db->dbprefix('products'));

        $query = $this->db->get();

        $solucion = $query->result();

       // $this->result = (object) $solucion;



        $count = count($solucion);

        if($count < 4){

        $archivos = array('estado'=> $estado );

        

        $this->product_model->update_estado($archivos,$id);

        redirect('admin/products');

        }else{

        $archivos = array(

       'estado'=> 0        

        );

        $this->product_model->update_estado($archivos,$id);

        redirect('admin/products');

        }



    }
    /*
     * Categorias
     */

    public function create_category() {
        $categories = $this->product_category_model->order_by('id', 'ASC')->get_all();
        $this->template
        ->set('categories', $categories)
        ->build('admin/create_category');
    }

    // -----------------------------------------------------------------

    public function store_category() {

        $this->form_validation->set_rules('title', 'Titulo', 'required|trim');
        $this->form_validation->set_rules('parent', 'Padre', '');

        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();

            $data = array(
                'title' => $post->title,
                'slug' => slug($post->title),
                'parent' => $post->parent
                //'created_at' => date('Y-m-d H:i:s')
                );

            if ($this->product_category_model->insert($data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/products#page-categories');
            } else {
                $this->session->set_flashdata('error', lang('galeria:error_message'));
                redirect('admin/products/create_category');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/products/create_category');
        }
    }

    // -----------------------------------------------------------------

    public function destroy_category($id = null) {
        $id or redirect('admin/products#page-categories');
        if ($this->product_category_model->delete($id)) {
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/products#page-categories');
    }

    // --------------------------------------------------------------------------------------

    public function edit_category($id = null) {
        $category = $this->product_category_model->get($id);
        $categories = $this->product_category_model->order_by('id', 'ASC')->get_all();
        $this->template
        ->set('categories', $categories)
        ->set('category', $category)
        ->build('admin/edit_category');
    }

    // -----------------------------------------------------------------

    public function update_category() {

        $this->form_validation->set_rules('title', 'Titulo', 'required|trim');
        $this->form_validation->set_rules('parent', 'Padre', '');

        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();

            $data = array(
                'title' => $post->title,
                'slug' => slug($post->title),
                'parent' => $post->parent
                );

            if ($this->product_category_model->update($post->id,$data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/products#page-categories');
            } else {
                $this->session->set_flashdata('error', lang('galeria:error_message'));
                redirect('admin/products/create_category');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/products/create_category');
        }
    }

    /*
     * Productos
     */

    public function create() {
        $categories = $this->product_category_model->order_by('id', 'ASC')->get_all();
        $this->template
        ->set('categories', $categories)
        ->build('admin/create');
    }

    // -----------------------------------------------------------------

    public function store() {

        // Validaciones del Formulario
        $this->form_validation->set_rules('name', 'Nombre', 'required|trim');
        $this->form_validation->set_rules('categories', 'Categorias');
        $this->form_validation->set_rules('content', 'Descripción', 'required|trim');
        $this->form_validation->set_rules('introduction', 'Introducción', 'required|trim');
      

        // Se ejecuta la validación
        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();

            // Array que se insertara en la base de datos
            $data = array(
                'name' => $post->name,
                'slug' => slug($post->name),
                'description' => html_entity_decode($post->content),
                'introduction' => $post->introduction,              
                //'created_at' => date('Y-m-d H:i:s')
                );

            // Se carga la imagen
            $config['upload_path'] = './' . UPLOAD_PATH . '/products';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['image']['name'];

            if (!empty($img)) {
                if ($this->upload->do_upload('image')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = UPLOAD_PATH . 'products/' . $datos['upload_data']['file_name'];
                    $img = array('image' => $path);
                    $data = array_merge($data, $img);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('admin/products/');
                }
            }

            // Se inserta en la base de datos
            if ($this->product_model->insert($data)) {
               
                $productId = $this->db->insert_id();
                $categories = $post->categories;

                // Se relacionan las categorias posteriormente a la inserción
                for($i=0; $i < count($categories); $i++){
                    $data = array(
                        'product_id' => $productId,
                        'category_id' => $categories[$i]
                        );
                    $this->db->insert($this->db->dbprefix.'products_categories', $data);
                }               

                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/products');
            } else {
                $this->session->set_flashdata('error', lang('galeria:error_message'));
                redirect('admin/products/create');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/products/create');
        }
    }

    // -----------------------------------------------------------------

    public function destroy($id = null) {
        $id or redirect('admin/products');
        $obj = $this->db->where('id', $id)->get($this->db->dbprefix.'products')->row();
        if ($this->product_model->delete($id)) {
            @unlink($obj->image); // Eliminamos archivo existente
            $this->db->delete($this->db->dbprefix.'products_categories', array('product_id' => $id)); // Eliminaos relación pro cat
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/products');
    }

    // --------------------------------------------------------------------------------------

    public function edit($id = null) {
        $id or redirect('admin/products');
        $product = $this->product_model->get($id);
        $categories = $this->product_category_model->order_by('id', 'ASC')->get_all();

        $return = $this->db->where('product_id',$id)->get('products_categories')->result();
        $selected_category = array();

        foreach ($return as $item) {
            $selected_category[] = $item->category_id;
        }

        $this->template
        ->set('categories', $categories)
        ->set('selected_category', $selected_category)
        ->set('product', $product)
        ->build('admin/edit');
    }

    // -----------------------------------------------------------------

    public function update() {

        // Validaciones del Formulario
        $this->form_validation->set_rules('name', 'Nombre', 'required|trim');
        $this->form_validation->set_rules('categories', 'Categorias');
        $this->form_validation->set_rules('content', 'Descripción', 'required|trim');
        $this->form_validation->set_rules('introduction', 'Introducción', 'required|trim');
      
        // Se ejecuta la validación
        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();

            // Array que se insertara en la base de datos
            $data = array(
                'name' => $post->name,
                'slug' => slug($post->name),
                'description' => html_entity_decode($post->content),
                'introduction' => $post->introduction,                
                );

            // Se carga la imagen
            $config['upload_path'] = './' . UPLOAD_PATH . '/products';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['image']['name'];

            if (!empty($img)) {
                if ($this->upload->do_upload('image')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = UPLOAD_PATH . 'products/' . $datos['upload_data']['file_name'];
                    $img = array('image' => $path);
                    $data = array_merge($data, $img);
                    $obj = $this->db->where('id', $post->id)->get('products')->row();
                    @unlink($obj->image);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('admin/products/');
                }
            }

            // Se inserta en la base de datos
            if ($this->product_model->update($post->id,$data)) {

                $this->db->delete($this->db->dbprefix.'products_categories', array('product_id' => $post->id)); // Eliminaos relación pro cat

                $categories = $post->categories;

                // Se relacionan las categorias posteriormente a la inserción
                for($i=0; $i < count($categories); $i++){
                    $data = array(
                        'product_id' => $post->id,
                        'category_id' => $categories[$i]
                        );
                    $this->db->insert($this->db->dbprefix.'products_categories', $data);
                }

                $this->session->set_flashdata('success', 'Los registros se Actualizaron con éxito.');
                redirect('admin/products');
            } else {
                $this->session->set_flashdata('error', lang('galeria:error_message'));
                redirect('admin/products/create');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/products/create');
        }
    }


    /*
     * Actualizar Intro
     */

    public function update_intro() {
        $this->form_validation->set_rules('content', 'Texto', 'required');
        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();
            $data = array(
                'text' => html_entity_decode($post->content)
                );
            if ($this->product_intro_model->update($post->id, $data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/products#page-intro');
            } else {
                $this->session->set_flashdata('success', lang('gallery:error_message'));
                redirect('admin/products#page-intro');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/products#page-intro');
        }
    }

    /*
     * Administración de imagenes
     */

    public function images($id = null) {
        $id or redirect('admin/products');
        // Se consultan las imagenes del product
        $images = $this->product_image_model->get_many_by("product_id",$id);
        $product = $this->product_model->get_many_by("id",$id)[0];

        $this->template
        ->set('product', $product)
        ->set('images', $images)
        ->build('admin/images');
    }

    // ----------------------------------------------------------------------------------

    public function create_image($id = null) {
        $id or redirect('admin/products');
        $product = $this->product_model->get_many_by("id",$id)[0];
        $this->template
        ->set('product', $product)
        ->build('admin/create_image');
    }

    // -----------------------------------------------------------------

    public function store_image() {

            // Se carga la imagen
        $config['upload_path'] = './' . UPLOAD_PATH . '/products';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2050;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

            // imagen uno
        $img = $_FILES['image']['name'];
        $image = array();
        $id = $this->input->post('id');

        if (!empty($img)) {
            if ($this->upload->do_upload('image')) {
                $datos = array('upload_data' => $this->upload->data());
                $path = UPLOAD_PATH . 'products/' . $datos['upload_data']['file_name'];
                $image = array(
                    'product_id' => $id,
                    'path' => $path
                    );
            } else {
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect('admin/products/images/'.$id);
            }
        }

            // Se inserta en la base de datos
        if ($this->product_image_model->insert($image)) {
            $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
            redirect('admin/products/images/'.$id);
        } else {
            $this->session->set_flashdata('error', lang('galeria:error_message'));
            redirect('admin/products/create_image/'.$id);
        }
    }

    // -----------------------------------------------------------------

    public function destroy_image($id = null,$product_id = null) {
        $id or redirect('admin/products');
        $product_id or redirect('admin/products');
        $obj = $this->product_image_model->get_many_by('id',$id)[0];
        if ($this->product_image_model->delete($id)) {
            @unlink($obj->path); // Eliminamos archivo existente
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/products/images/'.$product_id);
    }

}
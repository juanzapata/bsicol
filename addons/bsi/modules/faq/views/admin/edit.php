<section class="title">

    <h4>Preguntas Frecuentes</h4>

    <br>

    <small class="text-help">Los campos señalados con <span>*</span> son obligatorios.</small>

</section>



<section class="item">

    <div class="description">

        <div class="tabs">

            <ul class="tab-menu">

                <li><a href="#page-faqs"><?php echo $titulo; ?></a></li>

            </ul>



            <div class="form_inputs" id="page-faqs">

                <?php echo form_open_multipart(site_url('admin/faq/edit_faq/'.(isset($datosForm) ? $datosForm->id : '')), 'id="form-wysiwyg"'); ?>

                <div class="inline-form">

                    <fieldset>
<ul>                          

                        <li>

                            <label for="name">Pregunta<span>*</span></label>
                             <div class="input">
                            <div class="sroll-table">
                                <?php echo form_textarea(array('id' => 'pregunta', 'name' => 'pregunta', 'value' => (isset($datosForm->pregunta)) ? $datosForm->pregunta : set_value('pregunta'), 'rows' => 10)) ?>
                                <!--<input type="hidden" name="pregunta" id="text">-->
                            </div>
                        </div>
                        <br class="clear">
                     </li>
                   
                        <li>

                               <label for="name">Respuesta<span>*</span></label>
                             <div class="input">
                            <div class="sroll-table">
                                <?php echo form_textarea(array('id' => 'respuesta', 'name' => 'respuesta', 'value' => (isset($datosForm->respuesta)) ? $datosForm->respuesta : set_value('respuesta'), 'rows' => 10)) ?>
                                <!--<input type="hidden" name="respuesta" id="text">-->
                            </div>
                        </div>
                        <br class="clear">

                        </li>                        
              
                       </ul>
                      
                       <br class="clear">
                             <div class="buttons float-right padding-top">
                               <?php

                        $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));

                        ?>
         
                             </div>
                       </fieldset>
                         <?php echo form_close(); ?>
                       </div>
                     </div>
                 </div>
             </div>
    </section>











                                 
                              
      



              



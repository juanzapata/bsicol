<section class="title">

    <h4>Preguntas Frecuentes</h4>

</section>

<section class="item">

    <div class="content">

        <div class="tabs">

            <ul class="tab-menu">

                <li><a href="#page-faq"><span>Lista de Preguntas</span></a></li>

                

                

            </ul>



            <!-- PRODUCTOS -->

            <div class="form_inputs" id="page-faq">

                <fieldset>



                    <?php echo anchor('admin/faq/edit_faq', '<span>+ Nuevo</span>', 'class="btn blue"'); ?>

                    <br>

                    <br>



                    <?php if (!empty($faqs)): ?>



                        <table border="0" class="table-list" cellspacing="0">

                            <thead>

                                <tr>

                                    <th style="width: 40%"><span>Pregunta</span></th>

                                    <th style="width: 40%">Respuesta</th>                                   

                                    <th style="width: 20%">Acciones</th>  
                                

                                

                                </tr>

                            </thead>

                            <tfoot>

                                <tr>

                                    <td colspan="6">

                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>

                                    </td>

                                </tr>

                            </tfoot>

                            <tbody>

                                <?php foreach ($faqs as $f): ?>

                                    <tr>

                                         <td><?php echo substr(strip_tags($f->pregunta), 0,300) ?></td>

                                           <td><?php echo substr(strip_tags($f->respuesta), 0,300) ?></td>                                     

                                        
                                        <td>

                                            <?php echo anchor('admin/faq/edit_faq/' . $f->id, lang('global:edit'), 'class="btn green small"'); ?>

                                    

                                            <?php echo anchor('admin/faq/delete_faq/' . $f->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>                        

                                            
            

                                            

                                        </td>

                                    </tr>

                                <?php endforeach ?>

                            </tbody>

                        </table>



                    <?php else: ?>

                        <p style="text-align: center">No hay Registros actualmente</p>

                    <?php endif ?>

                </fieldset>

            </div>



            <!-- CATEGORIAS -->

           

</section>
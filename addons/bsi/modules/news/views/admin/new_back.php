<section class="title">

    <h4>Noticias/publicaciones</h4>

</section>

<section class="item">

    <div class="content">

        <div class="tabs">

            <ul class="tab-menu">

                <li><a href="#page-news"><span>Todas las Noticias</span></a></li>

            </ul>



            <!-- NOTICIAS -->



            <div class="form_inputs" id="page-news">

                <fieldset>

                    <?php echo anchor('admin/news/edit_new/', '<span>+Nuevo</span>', 'class="btn blue"'); ?>

                    <br>

                    <br>

                    <?php if (!empty($news)): ?>



                        <table border="0" class="table-list" cellspacing="0">

                            <thead>

                                <tr>

                                    <th style="width: 5%"></th>

                                    <th style="width: 20%">Imagen</th>

                                    <th style="width: 20%">Titulo</th>

                                    <th style="width: 35%">Introducción</th>

                                    <th class="width: 20%">Acciones</th>

                                </tr>

                            </thead>

                            <tfoot>

                                <tr>

                                    <td colspan="6">

                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>

                                    </td>

                                </tr>

                            </tfoot>

                            <tbody>

                            <?php $i = $pag + 1; ?>

                                <?php foreach ($news as $new): ?>

                                    <tr>

                                        <td><?php echo $i ?></td>

                                        <td>

                                            <?php if (!empty($new->image)): ?>

                                                <img src="<?php echo site_url($new->image); ?>" style="height: 130px;">

                                            <?php endif; ?>

                                        </td>

                                        <td><?php echo $new->title ?></td>

                                        <td><?php echo substr($new->introduction, 0,150) ?></td>

                                        <td>

                                            <?php echo anchor('admin/news/edit_new/' . $new->id, lang('global:edit'), 'class="btn green small"'); ?>

                                            <?php echo anchor('admin/news/delete_new/' . $new->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>

                                           <!-- <?php $count=0; ?>

                                            <?php 



                                                if($count < 5){



                                            if ($new->estado == 0) { ?>

                                                <a href="<?php echo site_url('admin/news/estado/1/' . $new->id) ?>" class="btn green">Destacar</a>

                                            <?php } else { ?>

                                                <a href="<?php echo site_url('admin/news/estado/0/' . $new->id) ?>" class="btn orange">Quitar destacado</a>

                                            <?php 

                                                    } 

                                                }else{



                                            if ($new->estado == 1) {?>



                                                <a href="<?php echo site_url('admin/news/estado/1/' . $new->id) ?>" class="btn orange">Quitar destacado</a>

                                            <?php } else { ?>

                                                

                                            <?php 

                                                    } 

                                                }?>-->

                                            

                                        </td>

                                    </tr>

                                    <?php $i++; ?>

                                <?php endforeach ?>

                            </tbody>

                        </table>



                    <?php else: ?>

                        <p style="text-align: center">No hay un servicio actualmente</p>

                    <?php endif ?>

                </fieldset>

            </div>

        </div>

    </div>

</section>
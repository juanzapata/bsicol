<!-- carousel -->
<div id="carousel-example-generic" class="carousel slide " data-ride="carousel">
	<!-- indicadores -->
	<ol class="carousel-indicators custom-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
		{{ banner }}
		<div class="item {{ if { helper:count identifier='asd' } == 1 }}active{{ endif }}">
			<a href="{{link}}"><img src="{{image}}" alt="{{title}}"></a>
			<div class="carousel-caption">
				<h3>{{title}}</h3>
				<p class="hidden-xs">{{text}}</p>
			</div>
		</div>
		{{ /banner }}
	</div>
	<!-- controls -->
	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>
<!-- Noticias - Servicios -->
<div class="row">
	<hr>
	<!-- Noticias destacadas -->
	<h2>Noticias Destacadas</h2>
	{{outstanding_news}}
	<div class="col-sx-12 col-sm-6 col-md-6 col-lg-6">
		<div class="cont-dest">
			<img src="{{image}}" alt="{{title}}">
			<div class="cont">
				<h4>{{title}}</h4>
				<p>{{text}}</p>
				<a href="{{link}}" class="btn btn-primary">Ver Mas</a>
			</div>
		</div>
	</div>
	{{/outstanding_news}}
</div>
<div class="row">
	<hr>
	<!-- Servicios Destacados -->
	<h2>Servicios Destacados</h2>
	{{outstanding_services}}
	<div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
		<div class="cont-dest">
			<img src="{{image}}" alt="{{title}}">
			<div class="cont">
				<h4>{{title}}</h4>
				<p>{{text}}</p>
				<a href="{{link}}" class="btn btn-primary">Ver Mas</a>
			</div>
		</div>
	</div>
	{{/outstanding_services}}
</div>
<!-- Servicios Destacados -->
	<h2>Nuestros Clientes</h2>
<div class="row">
	{{our_clients}}
		<div class="col-sm-6 col-md-3">
			<div class="thumbnail">
				<div style="overflow: hidden;max-height:170px;">
				<a href="{{link}}">	<img src="{{image}}"   class="img-responsive" style="min-width: 100%;"></a>
				</div>
				<!--<div class="caption">
					<!--<h4><?php echo substr($ourc->title, 0,45) ?></h4>
					<!-- <p><?php echo substr($ourc->text, 0, 133) ?></p>
					<p><a class="btn btn-primary btn-sm" href="<?php echo $ourc->link ?>" >Ver Mas</a></p>
				</div>-->
			</div>
		</div>
	{{ /our_clients}}
	</div>

</div>
<div class="row">
    <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12 tx-al-ri">
        <a class="btn btn-primary btn-sm back" href="javascript:history.back(1)"><span class="glyphicon glyphicon-arrow-left"></span> Volver</a>
    </div>
    <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12">
        <h1>{{ product.name }}</h1>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 alignleft">
        <div class="thumbnail img-float-left">
            <div style="overflow: hidden;max-height:400px;">
                <?php if(count($images) > 0){ ?>
                <div id="carousel-example-generic" class="carousel slide " data-ride="carousel" style="max-width: 460px">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        {{ images }}
                        <li data-target="#carousel-example-generic" data-slide-to="{{helper:count}}" {{ if { helper:count identifier='c' } == 1 }}class="active"{{ endif }} style="border: 1px solid #d7e0e2;"></li>
                        {{ /images }}
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        {{ images }}
                        <div class="item {{ if { helper:count identifier='c2' } == 1 }}active{{ endif }}" style="background-color: white !important;">
                           <img src="{{ path }}" data-src="holder.js/400x400" width="100%" alt="imagen" class="img-responsive">
                       </div>
                       {{ /images }}
                   </div>
               </div>
               <?php }else{ ?>
               <img src="{{ product.image }}" data-src="holder.js/400x400" width="100%" alt="" class="img-responsive">
               <?php } ?>
           </div>
       </div>
       <p>{{ product.description }}</p>
      
       <div class="row">
        <div class="col-md-12">
            <div class="well well-sm" style="float: left;margin-top: 2em;">
                <strong>Categorias relacionadas</strong><br>
                {{ categories }}
                <a href="products/index/{{ slug }}">{{ title }}</a>&nbsp;&nbsp;
                {{ /categories }}
            </div>
        </div>
    </div>
</div>
</div>
</div>


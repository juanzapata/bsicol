<div class="row">
    <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Productos {{ category }}</h2>
    </div>
</div>
<div class="row">
    <!--  Texto de introducción administrable -->
    <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12">
        <p>{{ intro.text }}</p>
    </div>
    <!-- Listado normal de categorias -->
    <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
        <div class="treemenu">
            {{ menu }}
        </div>
    </div>
    <!-- Select de categorias -->
    <div class="col-sm-6 col-md-3 visible-sm visible-xs">
        <div class="btn-group" style="margin-bottom: 10px;">
            <button type="button" class="btn btn-primary">Todas las Categorias</button>
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
            <ul class="dropdown-menu">
            {{ categories }}
            <li><a href="products/index/{{ slug }}">{{ title }}</a></li>
            {{ /categories }}
            </ul>
        </div>
        <div class="push"></div>
    </div>
    <div class="col-sm-6 col-md-9">
        <div class="row">
            {{ if products }}
            {{ products }}
            <div class="col-sx-12 col-sm-6 col-md-4 col-lg-4">
                <div class="cont-prod">
                    <div class="cont-img-prod">
                        <img src="{{image}}" alt="{{title}}">
                    </div>
                    <div class="cont">
                        <h4>{{ name }}</h4>
                        <p>{{ introduction }}</p>
                        <span class="price">{{ price }}</span>
                        <a href="{{ url }}" class="btn btn-primary">Ver Mas</a>
                    </div>
                </div>
            </div>
            {{ /products }}
            {{ else }}
            <div class="col-sm-12 col-md12"><p style="text-align:center;margin-top:80px"><strong>No se encontraron resultados...</strong></p></div>
            {{ endif }}
        </div>
    </div>
</div>

<!-- Necesario para los styles del Menú -->
<script>
$(".treemenu").children().attr("class","list-group");
$(".list-group").children().attr("class","list-group-item");
</script>
